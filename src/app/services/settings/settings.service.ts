import { Injectable, Inject } from '@angular/core';
import { Theme } from '../../interfaces/theme';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  theme: Theme = {
    url: 'assets/css/colors/default.css',
    name: 'default'
  };

  constructor(@Inject(DOCUMENT) private document) {
    this.chargerTheme();
   }

  saveTheme() {
    localStorage.setItem('theme', JSON.stringify(this.theme));
  }

  chargerTheme() {
    if (localStorage.getItem('theme')) {
      // console.log('charger depuis le local storage');
      this.theme = JSON.parse(localStorage.getItem('theme'));
      this.applyTheme(this.theme.name);
    } else {
      // console.log('use default value');
    }
  }

  applyTheme(themeName: string) {
    const url = `assets/css/colors/${themeName}.css`;
    // themeTemplate: viens de la Page index.html;
    this.document.getElementById('themeTemplate').setAttribute('href', url);

    this.theme.name = themeName;
    this.theme.url = url;

    this.saveTheme();
  }
}
