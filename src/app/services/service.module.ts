import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  SidebarService,
  SettingsService,
  SharedService
} from './service.index';

@NgModule({
  declarations: [],
  providers: [
    SidebarService,
    SettingsService,
    SharedService
  ],
  imports: [
    CommonModule
  ]
})
export class ServiceModule { }
