import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menus: any = [
    {
      title: 'Dashboard', icon: 'mdi mdi-gauge', subMenu: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Charts', url: '/charts' },
        { title: 'Settings', url: '/account-setting' }
      ]
    }
  ];

  constructor() { }
}
