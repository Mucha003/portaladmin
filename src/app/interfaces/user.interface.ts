// les Values Optionnel vont toujour a la Fin
export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    image?: string;
    role?: string;
    isGoogleAccount?: boolean;
}
