import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  porcent: number = 50;

  constructor() { }

  ngOnInit() {
  }

  Increment() {
    if (this.porcent === 100) {
      this.porcent = 100;
    } else {
      this.porcent = this.porcent + 5;
    }
  }

  Decrement() {
    if (this.porcent === 0) {
      this.porcent = 0;
    } else {
      this.porcent = this.porcent - 5;
    }
  }

}
