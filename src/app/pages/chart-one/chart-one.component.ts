import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart-one',
  templateUrl: './chart-one.component.html',
  styleUrls: ['./chart-one.component.css']
})
export class ChartOneComponent implements OnInit {

  charts: any = {
    'chartdoughnut': {
      'labels': ['nutela', 'miel', 'confiture'],
      'data':  [24, 30, 46],
      'type': 'doughnut',
      'legend': 'Le pain ce mange avec '
    },
    'chartPie': {
      'labels': ['Homme', 'Femmes'],
      'data':  [4500, 6000],
      'type': 'pie',
      'legend': 'Personnes'
    },
    'chartPolarArea': {
      'labels': ['Si', 'No'],
      'data':  [65, 35],
      'type': 'polarArea',
      'legend': 'est ce vrai'
    },
    'chartFinal': {
      'labels': ['No', 'Si'],
      'data':  [85, 15],
      'type': 'doughnut',
      'legend': 'nimporte Quoi'
    },
  };


  constructor() { }

  ngOnInit() {
  }

}
