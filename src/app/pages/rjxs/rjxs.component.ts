import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/observable';
import { Subscriber } from 'rxjs';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-rjxs',
  templateUrl: './rjxs.component.html',
  styles: []
})
export class RjxsComponent implements OnInit {

  constructor() {
    const obs$ = new Observable((observer: Subscriber<any>) => {
      let counter = 0;

      const interval = setInterval(() => {
        counter += 1;

        // next == permet de notifier a chaque fois qu'il y a une info qui arrive
        // a chaque fois que nous voulons notifier nos subscribers,
        // on utilise next.
        observer.next(counter);

        if (counter === 3) {
          observer.complete();
        }

        if (counter === 2) {
          observer.error('erreur de l observable');
        }
      }, 1000);
    });

    // pour utiliser cette observable il faut ce subscribe.
    // pipe : aide a transformer notre data
    obs$.pipe(
      retry(2) // 2: le nombre de retry qu'on vet
      ).subscribe(num => {
      console.log('Subscription', num);
    }, err => {
      console.log('Il y a eu une erreur', err);
    }, () => {
      console.log('tout est finit');
    });
  }

  ngOnInit() {
  }

}
