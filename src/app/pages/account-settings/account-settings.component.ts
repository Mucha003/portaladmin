import { Component, OnInit } from '@angular/core';
import { SettingsService } from './../../services/service.index';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: []
})
export class AccountSettingsComponent implements OnInit {

  constructor(public settingsService: SettingsService) { }

  ngOnInit() {
    this.placerCheck();
  }

  // link de type element ref
  changeColorTheme(nameColorTheme: string, link: any) {
    console.log(link);

    this.applyCheck(link); // petiit v sur les couleurs
    this.settingsService.applyTheme(nameColorTheme);
  }

  applyCheck(link: any) {
    const selectors: any = document.getElementsByClassName('selector');

    for (let item of selectors) {
      item.classList.remove('working');
    }

    link.classList.add('working');
  }

  placerCheck() {
    // tous les Balises qui ont cette class
    const selectors: any = document.getElementsByClassName('selector');
    const themeName = this.settingsService.theme.name;

    for (let item of selectors) {
      if(item.getAttribute('data-theme') === themeName) {
        item.classList.add('working');
        break;
      }
    }
  }

}
