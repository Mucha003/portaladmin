import { NgModule } from '@angular/core';

import { ChartsModule } from 'ng2-charts';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartOneComponent } from './chart-one/chart-one.component';
import { ProgressComponent } from './progress/progress.component';
import { SharedModule } from '../shared/shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { RjxsComponent } from './rjxs/rjxs.component';
import { ChartsComponent } from '../components/charts/charts.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';

@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    ChartOneComponent,
    ProgressComponent,
    ChartsComponent,
    RjxsComponent,
    AccountSettingsComponent,
  ],
  imports: [
    SharedModule,
    PagesRoutingModule,
    ChartsModule
  ],
  exports: [
    DashboardComponent,
    ChartOneComponent,
    ProgressComponent,
  ]
})
export class PagesModule { }
