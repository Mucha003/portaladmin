import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare function init_plugins();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    /*
      Ceci viens du param en haut et du file Js/custom.js
      qui sert a init les func Jquery dans le template
      cette methode sert a appeler une func js depuis angular.
      mm chose dans la page Page
    */
    init_plugins();
  }

  connection() {
    this.router.navigate(['/dashboard']);
  }

}
