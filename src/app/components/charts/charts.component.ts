import { Component, OnInit, Input } from '@angular/core';
import { ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styles: []
})
export class ChartsComponent implements OnInit {

  @Input() public ChartType: ChartType;
  @Input() public ChartLabels: Label[] = [];
  @Input() public ChartData: number[] = [];

  constructor() { }

  ngOnInit() {
  }

}
