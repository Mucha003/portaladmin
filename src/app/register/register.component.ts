import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import Swal from 'sweetalert2';

// sans ceci le loading va etre infinit
declare function init_plugins();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor() { }

  ngOnInit() {
    init_plugins();
    this.initRegisterForm();
  }

  initRegisterForm() {
    this.registerForm = new FormGroup({
      // tout les champs qui sont dans le Html
      // '' => default Value, Validations
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required),
      conditions: new FormControl(false),
    },
    // Validation peronnalisée
    { validators:  this.ValidatorConfirmPassword('password', 'confirmPassword')});
  }


  ValidatorConfirmPassword(password: string, confirmPassword: string) {
    return (group: FormGroup) => {

      const pass1 = group.controls[password].value;
      const pass2 = group.controls[confirmPassword].value;

      if (pass1 === pass2) {
        // Validation Ok
        return null;
      }

      // Validation Pas OKK
      return {
        samePassword: true
      };
    };
  }


  saveUser() {
    if (!this.registerForm.valid) {
      return;
    }

    if (!this.registerForm.value.conditions) {
      Swal.fire('Important', 'You must Accep All Terms', 'warning');
      return;
    }

    console.log(this.registerForm.valid);
    console.log(this.registerForm.value);
  }


}
